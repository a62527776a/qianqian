export default (url, method = 'GET', body = {}) => {
  return new Promise((resolve, reject) => {
    wx.request({
      url: 'https://dscsdoj.top' + url,
      // url: 'http://192.168.31.31:7001' + url,
      method: method,
      data: body,
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        resolve(res.data)
      },
      fail: function (e) {
        console.log(e)
        reject(e)
      },
      complete: function () {
        console.log('complete')
      }
      // transformRequest: [function (data) {
      //   // App.$store.commit('SETNETWORKSTATE', true)
      //   // console.log(store.state.networkloading.loading)
      //   return data
      // }]
    })
  })
}
