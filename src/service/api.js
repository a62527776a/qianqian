import core from './core'

export default {
  wxLogin: async code => core('/api/wx/login?code=' + code),
  findHotBackgroundImage: async page => core('/api/source/update?page=' + page),
  getHotTitle: async () => core('/api/title'),
  getAdState: async () => core('/api/ad'),
  getBookmark: async () => core('/api/bookmark'),
  findExampleByType: async (type) => core('/api/example/type?type=' + type),
  submitFormId: async data => core('/api/formid', 'POST', data)
}
