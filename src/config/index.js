export default {
  AppVersion: '0.4.1',
  updateInfo: '1. 增加文字颜色选择',
  historyUpdateInfo: [
    {
      AppVersion: '0.4.0',
      updateInfo: '1. 优化文字模块'
    },
    {
      AppVersion: '0.3.8',
      updateInfo: '1. 优化图片Feed流'
    },
    {
      AppVersion: '0.3.7',
      updateInfo: '1. 处理图片变形问题 2. 优化图片流程'
    },
    {
      AppVersion: '0.3.6',
      updateInfo: '1. 处理长图片变形问题'
    },
    {
      AppVersion: '0.3.5',
      updateInfo: '1. 优化编辑体验 2. 优化选图界面 3. 放大可编辑区域宽度'
    },
    {
      AppVersion: '0.3.4',
      updateInfo: '1. 优化手势放大字体 2. 优化选择模板策略 3. 完善分享内容 4. 增加横图模式 优化图片选择步数'
    },
    {
      AppVersion: '0.3.3',
      updateInfo: '1. 选模板按钮bug fix 2. 支持手势放大字体'
    },
    {
      AppVersion: '0.3.2',
      updateInfo: '1. 选模板界面展示优化 2. 增加保存图片按钮'
    },
    {
      AppVersion: '0.3.1',
      updateInfo: '1. 优化性能 2. 处理bug 3. 优秀范例展示'
    },
    {
      AppVersion: '0.3.0',
      updateInfo: '1. 更加友好的模板选择方式 2. 更加友好的主菜单'
    },
    {
      AppVersion: '0.2.9',
      updateInfo: '1. 增加导出长图 2. 替换更加美观的文字颜色 3. 增加阴影选项'
    },
    {
      AppVersion: '0.2.81',
      updateInfo: '1. 修改相关文案 增加提示性语句'
    },
    {
      AppVersion: '0.2.8',
      updateInfo: '1. 处理编辑栏textarea编辑异常 2. 处理广告样式'
    },
    {
      AppVersion: '0.2.7',
      updateInfo: '1. 处理动态传背景图片无法生效的BUG 2. 更新底部样式 3. 更新控制栏样式'
    },
    {
      AppVersion: '0.2.6',
      updateInfo: '1. 样式美化'
    },
    {
      AppVersion: '0.2.5',
      updateInfo: '1. 增加字间距调整'
    },
    {
      AppVersion: '0.2.4',
      updateInfo: '1. 接入CDN 2. 动态化界面 3. 优化选图体验'
    },
    {
      AppVersion: '0.2.3',
      updateInfo: '1. 清空文字按钮bug修复 2. 编辑文字界面可拖动文本框 3. 键盘不再遮挡文本框 4. 可编辑副文本文字颜色'
    },
    {
      AppVersion: '0.2.2',
      updateInfo: '1. 优化卡片模板对图片比例不正常的的裁剪 2. 增加清空文字按钮 3. 调整本地相册/拍照按钮位置 4. 修复无法展示热图栏的问题 5. 主栏按钮去重 6. 卡片阴影样式优化'
    },
    {
      AppVersion: '0.2.1',
      updateInfo: '1. 点击卡片模板的图片弹出选图界面 2. 主文字 副文字 根据屏幕密度调整渲染准确性 3. 卡片模板支持比例不正确的图片裁剪 4. 动态热图'
    },
    {
      AppVersion: '0.2.0',
      updateInfo: '1. 新增切换导出形态 2. 修复对长图的展示异常 3. 处理安卓部分机型文字划出屏幕外导致屏幕滚动异常 4. 新增副文字'
    },
    {
      AppVersion: '0.1.9',
      updateInfo: '1. 优化字体渲染偏移'
    },
    {
      AppVersion: '0.1.8',
      updateInfo: '1. 优化字体渲染偏移 2. 预设纯色背景'
    },
    {
      AppVersion: '0.1.7',
      updateInfo: '1. 增加框居中对齐功能 2. 处理文字对齐渲染偏移'
    },
    {
      AppVersion: '0.1.6',
      updateInfo: '1. 展示界面修改 2. 禁止iOS下input框与页面一起滚动'
    },
    {
      AppVersion: '0.1.5',
      updateInfo: '1. 处理绘制文字时的偏差 2. 放大文字范围 4. 导出图片时的细节修正 5.增加文字对齐方式'
    },
    {
      AppVersion: '0.1.4',
      updateInfo: '1. 用户可选供填写文字留白区域 2. 预设几张背景图片'
    },
    {
      AppVersion: '0.1.3',
      updateInfo: '1. 增加字体大小选项并处理改变字体大小后渲染的位置错误bug 2. 用户界面优化 增加直白的菜单栏标题'
    },
    {
      AppVersion: '0.1.2',
      updateInfo: '1. 优化传入背景图片时的操作体验 2. 处理绘制文字时的偏差 3. 制作完图片可以发送给好友 4. 新增几种可选文字颜色 5. 修改默认展示图片'
    }
  ]
}
