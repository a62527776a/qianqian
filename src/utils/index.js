import config from '../config'

function formatNumber (n) {
  const str = n.toString()
  return str[1] ? str : `0${str}`
}

export function formatTime (date) {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()

  const t1 = [year, month, day].map(formatNumber).join('-')
  return `${t1}`
}

/**
 * @method setReadUpdateInfo 设置阅读更新公告
 */
const setReadUpdateInfo = () => {
  wx.getStorage({
    key: 'AppVersion',
    success: (res) => {
      // 由于微信小程序自动更新
      // 存入版本号与当前版本号不一致 则展示当前版本更新信息
      if (res.data !== config.AppVersion) setUpdateStorage()
    },
    fail: (res) => {
      // 第一次进入小程序 版本信息不存在，则展示操作提示 并存入版本信息
      if (res.errMsg === 'getStorage:fail data not found') {
        wx.setStorage({
          key: 'AppVersion',
          data: config.AppVersion
        })
        wx.showModal({
          title: '操作提示',
          content: `hello~ 点击图片即可修改你喜欢的图片~ 点击对话框修改文字 拖动对话框还可将文字移动 其他按钮还可给你更多想要的~`,
          showCancel: false
        })
      }
    }
  })
}

/**
 * @method setUpdateStorage 存入版本数据进storage并通知用户更新信息
 */
const setUpdateStorage = () => {
  wx.setStorage({
    key: 'AppVersion',
    data: config.AppVersion
  })
  wx.showModal({
    title: '更新信息',
    content: `更新版本：v${config.AppVersion}。更新内容：${config.updateInfo}`,
    showCancel: false
  })
}

export default {
  formatNumber,
  formatTime,
  setReadUpdateInfo
}
