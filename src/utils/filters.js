// 多种滤镜
// 来源 https://blog.csdn.net/jia20003/article/details/9195915
// 来源 https://yi-jy.com/2015/07/26/canvas-image-filter/
export default {
  // 反色
  colorInvertProcess: (data) => {
    let len = data.length
    let r = 0
    let g = 0
    let b = 0
    for (let i = 0; i < len; i += 4) {
      r = data[i]
      g = data[i + 1]
      b = data[i + 2]

      data[i] = 255 - r
      data[i + 1] = 255 - g
      data[i + 2] = 255 - b
    }
    return data
  },
  // 灰度
  colorAdjustProcess: (data, size) => {
    let len = data.length
    for (var i = 0; i < len; i += 4) {
      let average = (data[i] + data[i + 1] + data[i + 2]) / 3
      data[i] = data[i + 1] = data[i + 2] = average
    }
    return data
  },
  // 复古
  sepia: (data) => {
    let len = data.length
    let r = 0
    let g = 0
    let b = 0
    for (var i = 0; i < len; i += 4) {
      r = data[i]
      g = data[i + 1]
      b = data[i + 2]
      data[i] = (r * 0.393) + (g * 0.769) + (b * 0.189)
      data[i + 1] = (r * 0.349) + (g * 0.686) + (b * 0.168)
      data[i + 2] = (r * 0.272) + (g * 0.534) + (b * 0.131)
    }
    return data
  },
  // 模糊
  blurProcess: (data, size) => {
    let sumred = 0.0
    let sumgreen = 0.0
    let sumblue = 0.0
    let idx = 0
    for (let x = 0; x < size.width; x++) {
      for (let y = 0; y < size.height; y++) {
        idx = (x + y * size.width) * 4
        for (let subCol = -2; subCol <= 2; subCol++) {
          let colOff = subCol + x
          if (colOff < 0 || colOff >= size.width) {
            colOff = 0
          }
          for (let subRow = -2; subRow <= 2; subRow++) {
            let rowOff = subRow + y
            if (rowOff < 0 || rowOff >= size.height) {
              rowOff = 0
            }
            let idx2 = (colOff + rowOff * size.width) * 4
            let r = data[idx2 + 0]
            let g = data[idx2 + 1]
            let b = data[idx2 + 2]
            sumred += r
            sumgreen += g
            sumblue += b
          }
        }
        let nr = (sumred / 25)
        let ng = (sumgreen / 25)
        let nb = (sumblue / 25)

        sumred = 0
        sumgreen = 0
        sumblue = 0

        data[idx + 0] = nr
        data[idx + 1] = ng
        data[idx + 2] = nb
        data[idx + 3] = 255
      }
    }
    return data
  },
  // 浮雕
  reliefProcess: (data, size) => {
    let len = data.length
    for (var i = 0; i < len; i++) {
      if ((i + 1) % 4 !== 0) {
        if ((i + 4) % (size.width * 4) === 0) {
          data[i] = data[i - 4]
          data[i + 1] = data[i - 3]
          data[i + 2] = data[i - 2]
          data[i + 3] = data[i - 1]
          i += 4
        } else {
          data[i] = 255 / 2 + 2 * data[i] - data[i + 4] - data[i + size.width * 4]
        }
      } else {
        if ((i + 1) % 4 !== 0) {
          data[i] = data[i - size.width * 4]
        }
      }
    }
    return data
  },
  // 阈值
  threshold: (data) => {
    let len = data.length
    for (var i = 0; i < len; i += 4) {
      let average = (data[i] + data[i + 1] + data[i + 2]) / 3
      // https://yi-jy.com/2015/07/26/canvas-image-filter/ 下的阈值解释
      /**
       * 何为阈值，一开始我也是不太了解。但当你看到上面的效果时，你就会明白了。网上的解释是：“阈值”命令将灰度或彩色图像转换为高对比度的黑白图像。您可以指定某个色阶作为阈值。所有比阈值亮的像素转换为白色；而所有比阈值暗的像素转换为黑色。“阈值”命令对确定图像的最亮和最暗区域很有用。
       * 那么问题来了，如何才能做出阈值的效果呢？
       * 想要得到阈值的效果，可以将灰度值（r、g、b三个点的平均值）与设定的阈值比较，如果大于等于阈值，则将该点设置为255，否则设置为0。
       */
      data[i] = data[i + 1] = data[i + 2] = average > 127.5 ? 255 : 0
    }
    return data
  },
  // 雕刻效果
  diaokeProcess: (data, size) => {
    for (let x = 1; x < size.width - 1; x++) {
      for (let y = 1; y < size.height - 1; y++) {
        let idx = (x + y * size.width) * 4
        let bidx = ((x - 1) + y * size.width) * 4
        let aidx = ((x + 1) + y * size.width) * 4

        let nr = data[bidx + 0] - data[aidx + 0] + 128
        let ng = data[bidx + 1] - data[aidx + 1] + 128
        let nb = data[bidx + 2] - data[aidx + 2] + 128

        nr = (nr < 0) ? 0 : ((nr > 255) ? 255 : nr)
        ng = (ng < 0) ? 0 : ((ng > 255) ? 255 : ng)
        nb = (nb < 0) ? 0 : ((nb > 255) ? 255 : nb)

        data[idx + 0] = nr
        data[idx + 1] = ng
        data[idx + 2] = nb
        data[idx + 3] = 255
      }
    }
    return data
  }
}
