/**
 * @constructor bookmark 书签
 * @var { Boolean } showPreview 展示预览Canvas
 * @var { String } backgroundImage 书签的背景
 * @var { String } content 书签的正文
 * @var { Date } createTime 书签的创作时间
 * @var { Boolean } whiteSpace 书签底部供放置文字的留白区域
 * @var { String } originSrc 图片在选择展示处理之前的原图路径
 * @var { String } model 样式模式 full / thumb
 * @var { Boolean } isVertical 是否为竖图
 * @var { Boolean } isDoublePoint 双指触控 用于放大字体
 * @var { Boolean } showText 是否展示文字
 * @var { Number } mode 模板
 * @var { Object } thumbStyle 当model为thumb时的样式
 * @const { Function } SET_BOOKMARK_CONTENT 设置当前书签的内容
 * @const { Function } SET_DOUBLEPOINT 设置是否为双指操作
 * @const { Function } SET_BACKGROUND_IMAGE 设置当前书签的背景图片
 * @const { Function } SET_BOOKMARK_COLOR 设置当前书签文字颜色
 * @const { Function } SET_BOOKMARK_BACKGROUND_SIZE 设置书签背景图片尺寸
 * @const { Function } SET_ORIGINSRC 设置原背景图的路径
 * @const { Function } SET_TEXTALIGN 设置文字居中 居左 居右
 * @const { Function } SET_BOOKMARK_MODEL 设置签签样式
 * @const { Function } CHANGE_SHOW_SUB 是否展示副文字
 * @const { Function } SET_LINEHEIGHT 设置字间距
 * @const { Function } SET_SHOW_TEXT 设置是否展示文字
 * @const { Function } SET_TEXT_SHADOW 设置字体阴影
 * @const { Function } SET_MODE 设置模板
 * @const { Function } SET_VERTICAL 设置是否为竖图
 */
import { formatTime } from '../utils'

export const SET_BOOKMARK_CONTENT = 'SET_BOOKMARK_CONTENT'
export const SET_SUB_BOOKMARK_CONTENT = 'SET_SUB_BOOKMARK_CONTENT'
export const SET_CANVAS_PREVIEW = 'SET_CANVAS_PREVIEW'
export const SET_BACKGROUND_IMAGE = 'SET_BACKGROUND_IMAGE'
export const SET_BOOKMARK_COLOR = 'SET_BOOKMARK_COLOR'
export const SET_BOOKMARK_SUB_COLOR = 'SET_BOOKMARK_SUB_COLOR'
export const SET_BOOKMARK_TEXT_X = 'SET_BOOKMARK_TEXT_X'
export const SET_SUB_BOOKMARK_TEXT_X = 'SET_SUB_BOOKMARK_TEXT_X'
export const SET_DOUBLEPOINT = 'SET_DOUBLEPOINT'
export const SET_BOOKMARK_TEXT_Y = 'SET_BOOKMARK_TEXT_Y'
export const SET_SUB_BOOKMARK_TEXT_Y = 'SET_SUB_BOOKMARK_TEXT_Y'
export const SET_BOOKMARK_FONTWEIGHT = 'SET_BOOKMARK_FONTWEIGHT'
export const SET_BOOKMARK_FONTSIZE = 'SET_BOOKMARK_FONTSIZE'
export const SET_BOOKMARK_BACKGROUND_SIZE = 'SET_BOOKMARK_BACKGROUND_SIZE'
export const SET_ORIGINSRC = 'SET_ORIGINSRC'
export const SET_WHITESPACE = 'SET_WHITESPACE'
export const SET_TEXTALIGN = 'SET_TEXTALIGN'
export const SET_BOOKMARK_MODEL = 'SET_BOOKMARK_MODEL'
export const CHANGE_SHOW_SUB = 'CHANGE_SHOW_SUB'
export const SET_LINEHEIGHT = 'SET_LINEHEIGHT'
export const SET_TEXT_SHADOW = 'SET_TEXT_SHADOW'
export const SET_MODE = 'SET_MODE'
export const SET_VERTICAL = 'SET_VERTICAL'
export const SET_SHOW_TEXT = 'SET_SHOW_TEXT'
export const SET_OPENID = 'SET_OPENID'
// 文字居中 居左 居右选项
const TEXTALIGN_OPTIONS = {
  '左': {
    text: '左',
    title: '左对齐',
    name: 'left'
  },
  '中': {
    text: '中',
    title: '中对齐',
    name: 'center'
  },
  '右': {
    text: '右',
    title: '右对齐',
    name: 'right'
  }
}

const modelOpt = {
  full: 'full',
  thumb: 'thumb'
}

const windowSize = wx.getSystemInfoSync()

// 各个模板文字的x y 轴位置
const modeCtxTextPosition = {
  1: {
    x: windowSize.windowWidth * -0.04,
    y: (windowSize.windowHeight * 0.615)
  },
  2: {
    x: windowSize.windowWidth * -0.01,
    y: windowSize.windowHeight * 0.46
  },
  3: {
    x: windowSize.windowWidth * -0.06,
    y: windowSize.windowHeight * 0.666
  },
  4: {
    x: windowSize.windowWidth * -0.07,
    y: windowSize.windowHeight * 0.54
  }
  // 1: {
  //   x: windowSize.windowWidth * -0.04,
  //   y: windowSize.windowHeight * 0.615
  // },
  // 2: {
  //   x: windowSize.windowWidth * 0,
  //   y: windowSize.windowHeight * 0.471
  // },
  // 3: {
  //   x: windowSize.windowWidth * -0.06,
  //   y: windowSize.windowHeight * 0.637
  // },
  // 4: {
  //   x: windowSize.windowWidth * -0.07,
  //   y: windowSize.windowHeight * 0.528
  // }
}

export default {
  state: {
    openId: '',
    showPreview: false,
    originSrc: '',
    whiteSpace: false,
    isVertical: false,
    showText: false,
    mode: 1,
    showSub: false,
    model: 'thumb',
    isDoublePoint: false,
    placeholderText: '不知为什么\n只要有你在我身边\n我的心便不再惶惶不安',
    subPlaceholderText: formatTime(new Date()),
    logoTempFilePath: '',
    thumbStyle: {
      thumbBoxStyle: {
        top: windowSize.windowHeight * 0.19,
        height: windowSize.windowHeight * 0.4,
        left: windowSize.windowWidth * 0.03,
        width: windowSize.windowWidth * 0.94
      },
      thumbImageStyle: {
        width: (windowSize.windowWidth * 0.94) - (windowSize.windowHeight * 0.04),
        height: windowSize.windowHeight * 0.36,
        top: windowSize.windowHeight * 0.21,
        left: windowSize.windowWidth * 0.03 + windowSize.windowHeight * 0.02
      },
      top: windowSize.windowHeight * 0.17,
      // 卡片高度
      height: windowSize.windowHeight * 0.45,
      // 相框高度
      frameHeight: windowSize.windowHeight * 0.56,
      // 如果为竖图 则额外需要多出的高度
      verticalExtraHeight: 0,
      verticalExtraTop: 0
    },
    backgroundImage: {
      ctx: '../../static/default.jpg',
      style: {
        // height: windowSize.windowHeight * 0.9,
        height: 249,
        width: windowSize.windowWidth,
        // top: 0,
        top: ((windowSize.windowHeight * 0.9) - 249) / 2
      }
    },
    content: {
      ctx: '',
      style: {
        fontSize: 18,
        color: 'rgb(0, 243, 156)',
        x: modeCtxTextPosition[1].x,
        shadow: true,
        y: modeCtxTextPosition[1].y,
        fontWeight: 'normal',
        fontFamily: 'Helvetica Neue',
        textAlign: TEXTALIGN_OPTIONS['左'],
        lineHeight: 1.2,
        start: {
          x: 0,
          y: 0
        },
        move: {
          x: modeCtxTextPosition[1].x,
          y: modeCtxTextPosition[1].y
        },
        doubleStart: {
          point1: {
            x: 0,
            y: 0
          },
          point2: {
            x: 0,
            y: 0
          }
        },
        doubleMove: {
          point1: {
            x: 0,
            y: 0
          },
          point2: {
            x: 0,
            y: 0
          }
        }
      }
    },
    subContent: {
      ctx: '',
      style: {
        fontSize: 12,
        color: 'black',
        x: 20,
        y: 80,
        fontWeight: 'normal',
        // fontFamily: 'Helvetica Neue',
        // textAlign: TEXTALIGN_OPTIONS['中']
        start: {
          x: 0,
          y: 0
        },
        move: {
          x: 0,
          y: 0
        }
      }
    },
    createTime: new Date(),
    createTimeFormat: 'HH:mm:ss'
  },
  getters: {
    // 获取格式化后时间
    getCreateBookMarkTime: state => {
      // return moment(state.createTime).format(state.createTimeFormat)
    }
  },
  mutations: {
    SET_OPENID (state, openId) {
      state.openId = openId
    },
    // 总共分为4种模板
    /**
     * @param { Number } type 模板类型 1/2/3/4
     */
    [SET_MODE] (state, type) {
      if (type === 1) {
        state.model = 'thumb'
        state.whiteSpace = false
        state.mode = type
        state.content.style.x = state.content.style.move.x = modeCtxTextPosition[type].x
        state.content.style.y = state.content.style.move.y = modeCtxTextPosition[type].y
      }
      if (type === 2) {
        state.model = 'thumb'
        state.whiteSpace = true
        state.mode = type
        state.content.style.x = state.content.style.move.x = modeCtxTextPosition[type].x
        state.content.style.y = state.content.style.move.y = modeCtxTextPosition[type].y
      }
      if (type === 3) {
        state.model = 'full'
        state.whiteSpace = true
        state.mode = type
        state.content.style.x = state.content.style.move.x = modeCtxTextPosition[type].x
        state.content.style.y = state.content.style.move.y = modeCtxTextPosition[type].y
      }
      if (type === 4) {
        state.model = 'full'
        state.whiteSpace = false
        state.mode = type
        state.content.style.x = state.content.style.move.x = modeCtxTextPosition[type].x
        state.content.style.y = state.content.style.move.y = modeCtxTextPosition[type].y
      }
    },
    [SET_VERTICAL] (state) {
      state.isVertical = !state.isVertical
    },
    [SET_DOUBLEPOINT] (state, isDouble) {
      state.isDoublePoint = isDouble
    },
    [SET_TEXT_SHADOW] (state) {
      state.content.style.shadow = !state.content.style.shadow
    },
    [SET_LINEHEIGHT] (state, lineHeight) {
      state.content.style.lineHeight = lineHeight
    },
    [SET_SHOW_TEXT] (state) {
      state.showText = !state.showText
    },
    [CHANGE_SHOW_SUB] (state) {
      state.showSub = !state.showSub
    },
    /**
     * @param { String } position 居左 居右 还是居中
     * left/right/center
     */
    [SET_TEXTALIGN] (state, position) {
      try {
        state.content.style.textAlign = TEXTALIGN_OPTIONS[position]
      } catch (e) {
        console.error('textAlign 参数不合法')
      }
    },
    /**
     * @param { Boolean } boolean 是否展示留白区域
     */
    [SET_WHITESPACE] (state, boolean) {
      state.whiteSpace = boolean
    },
    /**
     * @param { String } weight 粗或细 bold / normal
     */
    [SET_BOOKMARK_FONTWEIGHT] (state) {
      state.content.style.fontWeight = state.content.style.fontWeight === 'bold' ? 'normal' : 'bold'
    },
    /**
     * @param path 原背景图的路径
     */
    [SET_ORIGINSRC] (state, path) {
      // 根据读取的图片大小数据来控制宽高
      wx.showLoading({
        title: '加载中...',
        mask: true
      })
      wx.getImageInfo({
        src: path,
        success: (res) => {
          let scale = res.width / res.height
          let height = ((windowSize.windowWidth * 0.94) - (windowSize.windowHeight * 0.04)) / scale
          state.thumbStyle.thumbBoxStyle.height = height + (windowSize.windowHeight * 0.04)
          state.thumbStyle.thumbImageStyle.height = height
          state.thumbStyle.frameHeight = state.thumbStyle.height = height + (windowSize.windowHeight * 0.09)
          state.thumbStyle.top = (windowSize.windowHeight * 0.85 - height) / 2
          state.thumbStyle.thumbBoxStyle.top = ((windowSize.windowHeight * 0.85 - height) / 2) + windowSize.windowHeight * 0.02
          state.thumbStyle.thumbImageStyle.top = ((windowSize.windowHeight * 0.85 - height) / 2) + windowSize.windowHeight * 0.04
          state.originSrc = path
          wx.hideLoading()
          wx.navigateBack()
        }
      })
    },
    /**
     * @param { String } content 书签内容
     */
    [SET_BOOKMARK_CONTENT] (state, content) {
      state.content.ctx = content
    },
    [SET_SUB_BOOKMARK_CONTENT] (state, subContent) {
      state.subContent.ctx = subContent
    },
    /**
     * @param { Boolean } isShow 是否展示预览图
     */
    [SET_CANVAS_PREVIEW] (state, isShow) {
      state.showPreview = isShow
    },
    /**
     * @param { String } path 用户选中的图片
     */
    [SET_BACKGROUND_IMAGE] (state, path) {
      state.backgroundImage.ctx = path
    },
    /**
     * @param { String } color 用户选中的颜色
     */
    [SET_BOOKMARK_COLOR] (state, color) {
      state.content.style.color = color
    },
    [SET_BOOKMARK_SUB_COLOR] (state, color) {
      state.subContent.style.color = color
    },
    [SET_BOOKMARK_TEXT_X] (state, x) {
      state.content.style.x = x
    },
    [SET_SUB_BOOKMARK_TEXT_X] (state, x) {
      state.subContent.style.x = x
    },
    [SET_BOOKMARK_TEXT_Y] (state, y) {
      state.content.style.y = y
    },
    [SET_SUB_BOOKMARK_TEXT_Y] (state, y) {
      state.subContent.style.y = y
    },
    // 设置当前内容的字体大小
    [SET_BOOKMARK_FONTSIZE] (state, px) {
      if (px < 8 || px > 42) return
      state.content.style.fontSize = px
    },
    /**
     * @function SET_BOOKMARK_BACKGROUND_SIZE 设置背景图片尺寸
     * @param { Object } obj 宽高
     * {
     *   height: height,
     *   width: width
     * }
     */
    [SET_BOOKMARK_BACKGROUND_SIZE] (state, obj) {
      state.backgroundImage.style = obj
    },
    [SET_BOOKMARK_MODEL] (state, model) {
      try {
        state.model = modelOpt[model]
      } catch (e) {
        throw new Error('设置模式错误 支持只包括 full thumb')
      }
    }
  },
  actions: {
    SET_OPENID ({commit}, openId) {
      commit('SET_OPENID', openId)
    },
    SET_DOUBLEPOINT ({ commit }, isDouble) {
      commit('SET_DOUBLEPOINT', isDouble)
    },
    SET_MODE ({commit}, type) {
      commit('SET_MODE', type)
    },
    SET_TEXT_SHADOW ({ commit }) {
      commit('SET_TEXT_SHADOW')
    },
    CHANGE_SHOW_SUB ({ commit }) {
      commit('CHANGE_SHOW_SUB')
    },
    SET_SHOW_TEXT ({ commit }) {
      commit('SET_SHOW_TEXT')
    },
    SET_TEXTALIGN ({ commit }, position) {
      commit('SET_TEXTALIGN', position)
    },
    SET_BOOKMARK_FONTWEIGHT ({ commit }) {
      commit('SET_BOOKMARK_FONTWEIGHT')
    },
    SET_WHITESPACE ({ commit }, boolean) {
      commit('SET_WHITESPACE', boolean)
    },
    SET_BOOKMARK_CONTENT ({ commit }, content) {
      commit('SET_BOOKMARK_CONTENT', content)
    },
    SET_SUB_BOOKMARK_CONTENT ({ commit }, subContent) {
      commit('SET_SUB_BOOKMARK_CONTENT', subContent)
    },
    SET_BACKGROUND_IMAGE ({ commit }, path) {
      commit('SET_BACKGROUND_IMAGE', path)
    },
    SET_BOOKMARK_COLOR ({ commit }, color) {
      commit('SET_BOOKMARK_COLOR', color)
    },
    SET_BOOKMARK_SUB_COLOR ({commit}, color) {
      commit('SET_BOOKMARK_SUB_COLOR', color)
    },
    SET_BOOKMARK_TEXT_X ({ commit }, X) {
      commit('SET_BOOKMARK_TEXT_X', X)
    },
    SET_SUB_BOOKMARK_TEXT_X ({ commit }, X) {
      commit('SET_SUB_BOOKMARK_TEXT_X', X)
    },
    SET_BOOKMARK_TEXT_Y ({ commit }, y) {
      commit('SET_BOOKMARK_TEXT_Y', y)
    },
    SET_SUB_BOOKMARK_TEXT_Y ({ commit }, Y) {
      commit('SET_SUB_BOOKMARK_TEXT_Y', Y)
    },
    SET_BOOKMARK_FONTSIZE ({commit}, px) {
      commit('SET_BOOKMARK_FONTSIZE', px)
    },
    SET_BOOKMARK_BACKGROUND_SIZE ({ commit }, obj) {
      commit('SET_BOOKMARK_BACKGROUND_SIZE', obj)
    },
    SET_ORIGINSRC ({commit}, path) {
      commit('SET_ORIGINSRC', path)
    },
    SET_BOOKMARK_MODEL ({ commit }, model) {
      commit('SET_BOOKMARK_MODEL', model)
    },
    SET_LINEHEIGHT ({ commit }, lineHeight) {
      commit('SET_LINEHEIGHT', lineHeight)
    },
    SET_VERTICAL ({ commit }) {
      commit('SET_VERTICAL')
    }
  }
}
